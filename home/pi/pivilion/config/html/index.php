<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome to Pivilion</title>
<style type="text/css">
	html {
		background-image: url("pi-logo_128.png");
		padding: 10px;
	}
	body {
		font-family: monospace;
		font-size: 14px;
		color: white;
		background-color: black;
		padding:10px;
		display: table;
	}
	a {
		font-weight: bold;
		text-decoration: underline;
		color: white;
	}

</style>
</head>
<body>
	<h1>Welcome to Pivilion!</h1>
	<p>Access your Rpi using SFTP or consult the manual to get your content here. </p>
	<p>For more info visit <a href="http://pivilion.net">pivilion.net</a>
</body>
</html> 
